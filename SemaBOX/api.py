from flask import Flask, render_template
from scapy.all import ARP, Ether, srp
import time
import socket
import requests
import speedtest

app = Flask(__name__)

def latency_test():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(5)
    
    start = time.time()
    try:
        s.connect(("google.com", 80))
    except socket.timeout:
        return -1
    end = time.time()
    s.close()

    return int((end - start) * 1000)

def scan_network():
    target_ip = "172.20.10.0/24" # Replace with your network address
    timeout = 2
    
    # Create ARP request for the target IP
    arp = ARP(pdst=target_ip)
    
    # Create Ethernet frame with broadcast MAC address
    ether = Ether(dst="ff:ff:ff:ff:ff:ff")
    
    # Combine ARP request and Ethernet frame to form a packet
    packet = ether/arp
    
    # Send packet and receive response
    result = srp(packet, timeout=timeout, verbose=0)[0]
    
    # Store IP and MAC addresses of connected devices
    devices = []
    for sent, received in result:
        devices.append({'ip': received.psrc, 'mac': received.hwsrc})
    
    # Return list of connected devices
    return devices

def check_internet_connection():
    try:
        # On essaye de se connecter à un serveur DNS de Google
        s = socket.create_connection(('8.8.8.8', 53), 2)
        s.close()
        return True
    except:
        return False

def get_public_ip():
    public_ip = requests.get('https://api.ipify.org').text
    return public_ip

def get_local_ip():
    """
    Récupère l'adresse IP locale de la machine
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()
    return local_ip

def get_hostname():
    """
    Récupère le nom d'hostname de la machine sur laquelle le code est exécuté.
    """
    return socket.gethostname()

def upload_speed():
    # create speedtest object
    st = speedtest.Speedtest()

    # get best server
    st.get_best_server()


    # test upload speed
    upload_speed = st.upload() / 1000000  # convert to megabits per second

    # return results with 2 decimal places
    return "{:.2f}".format(upload_speed)

def download_speed():
    # create speedtest object
    st = speedtest.Speedtest()

    # get best server
    st.get_best_server()

    # test download speed
    download_speed = st.download() / 1000000  # convert to megabits per second
    
    # return results with 2 decimal places
    return "{:.2f}".format(download_speed)

@app.route('/')
def index():
    latency = latency_test()
    connected = check_internet_connection()
    devices = scan_network()
    public_ip = get_public_ip()
    local_ip = get_local_ip()
    hostname = get_hostname()
    download = download_speed()
    upload = upload_speed()
    return render_template('index.html', latency=latency, connected=connected, devices=devices, public_ip=public_ip, local_ip=local_ip, hostname=hostname, download=download, upload=upload)

if __name__ == '__main__':
    app.run(host='127.0.0.1',port=5000,)
