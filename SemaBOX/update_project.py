import os
import subprocess

# Définition des paramètres
gitlab_url = 'https://gitlab.com/mikenzeuyem/semaos.git'
os_version_file = '/etc/os-version'
local_repo_dir = '/opt/mon_projet'

# Récupération de la version actuelle du OS
with open(os_version_file, 'r') as f:
    current_os_version = f.read().strip()

# Vérification de la dernière version disponible sur le dépôt GitLab
cmd = f'git ls-remote --tags {gitlab_url}'
output = subprocess.check_output(cmd, shell=True, universal_newlines=True)
latest_tag = output.split()[-1].split('/')[-1]  # récupération du dernier tag
latest_os_version = latest_tag.strip('v')

# Mise à jour si nécessaire
if latest_os_version != current_os_version:
    print(f'Mise à jour de la version {current_os_version} à {latest_os_version}')
    cmd = f'git clone {gitlab_url} {local_repo_dir}'
    subprocess.check_call(cmd, shell=True)
    cmd = f'cd {local_repo_dir} && git checkout tags/v{latest_os_version}'
    subprocess.check_call(cmd, shell=True)
    # Éventuelles autres commandes pour installer la nouvelle version du OS
else:
    print('La version actuelle est à jour')